using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Scriptable", menuName = "Scriptables/Scriptable")]
public class Scriptable : ScriptableObject
{
    public int num;
}
