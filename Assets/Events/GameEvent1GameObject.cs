using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/GameEvent - 1 gameobject")]
public class GameEvent1GameObject : GameEvent<GameObject> { }
