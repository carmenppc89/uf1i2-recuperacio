using System.Collections;
using System.Collections.Generic;
using Unity.Burst.Intrinsics;
using UnityEngine;


public class PlayerController : MonoBehaviour
{
    private SpriteRenderer spriteR;
    private Rigidbody2D rb;
    private Animator anim;
    [SerializeField] List<AnimationClip> animaClip;

    [SerializeField] private float speed;
    private Vector3 total;

    private enum States { Idle, Run, Attack };
    private States m_CurrentState;
    private float m_StateDeltaTime;
    private Coroutine m_HitComboTimeCoroutine;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        spriteR = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();

        Debug.Log(string.Format("Awake: rb -> {0}, spriteR -> {1}, anim -> {2}", rb, spriteR, anim));
    }
    private void Start()
    {
        Debug.Log("Start");
        InitState(States.Idle);
    }
    private void Update()
    {
        UpdateState(m_CurrentState);

        Vector3 moveDirection = Vector3.zero;

        if (Input.GetKey(KeyCode.W))
        {
            moveDirection.y += +1f;
            UpdateState(States.Run);
        }
        if (Input.GetKey(KeyCode.S))
        {
            moveDirection.y += -1f;
            UpdateState(States.Run);
        }
        if (Input.GetKey(KeyCode.A))
        {
            moveDirection.x += -1f;
            UpdateState(States.Run);
        }
        if (Input.GetKey(KeyCode.D))
        {
            moveDirection.x += +1f;
            UpdateState(States.Run);
        }

        total = moveDirection * speed * Time.deltaTime;


        transform.position += total;
    }

    private void InitState(States initState)
    {
        Debug.Log("InitState: " + initState);
        m_CurrentState = initState;
        m_StateDeltaTime = 0; //IMPORTANTE EL RESET

        //Animator.Play() siempre en el init
        switch (m_CurrentState)
        {
            case States.Idle:
                Debug.Log("idle start");
                anim.Play("Idle");
                break;
            case States.Run:
                Debug.Log("run start");
                anim.Play("Run");
                break;
            case States.Attack:
                Debug.Log("attack start");
                anim.Play("Attack");
                break;
            default:
                break;
        }
    }

    private void ExitState(States exitState)
    {
        switch (exitState)
        {
            case States.Idle:
                Debug.Log("Idle exit");
                break;
            case States.Run:
                Debug.Log("Run exit");
                break;
            case States.Attack:
                Debug.Log("Attack exit");
                break;
            default:
                break;
        }
    }

    private void ChangeState(States newState)
    {
        Debug.Log("ChangeState: " + newState.ToString() + ", current: " + m_CurrentState);
        if (newState == m_CurrentState) { return; }

        if (m_CurrentState == States.Idle || m_CurrentState == States.Attack || m_CurrentState == States.Run)
        {
            ExitState(m_CurrentState); //IMPORTANTE PRIMERO EL EXIT
        }


        InitState(newState);
    }

    private void UpdateState(States updateState)
    {
        m_StateDeltaTime += Time.deltaTime;

        switch (updateState)
        {
            case States.Idle:
                if (total.x != 0 && total.y != 0)
                    ChangeState(States.Run);

                if (Input.GetKeyDown(KeyCode.Mouse0))
                    ChangeState(States.Attack);
                break;

            case States.Run:
                if (total.x == 0 && total.y == 0)
                    ChangeState(States.Idle);

                if (Input.GetKeyDown(KeyCode.Mouse0))
                    ChangeState(States.Attack);
                break;

            case States.Attack:
                if (m_StateDeltaTime >= 1f)
                    ChangeState(States.Idle);
                break;

            default:
                break;
        }
    }
}
