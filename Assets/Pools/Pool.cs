using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Pool : MonoBehaviour
{
    [SerializeField]
    public GameObject[] PoolableObjects;
    private GameObject element;
    [SerializeField]
    public int Capacity;
    private List<GameObject> m_Pool;
    //public List<GameObject> Poolly { get => m_Pool; }

    private Transform trans;

    // Start is called before the first frame update
    void Awake()
    {
        //inicialitzem la llista de valors (m_Pool)
        m_Pool = new List<GameObject>();

        for (int i = 0; i < Capacity; i++)
        {
            element = Instantiate(PoolableObjects[Random.Range(0, PoolableObjects.Length)], transform);
            element.name = "thing - " + i;
            element.SetActive(false);
            m_Pool.Add(element);
        }
        //element.GetComponent<Poolable>().SetPool(this);
    }

    public GameObject GetElement()
    {
        foreach (GameObject element in m_Pool)
        {
            if (!element.activeInHierarchy)
            {
                Debug.Log(element.name + " -> GET");
                element.SetActive(true);
                return element;
            }
        }

        Debug.Log("EXCEPTION POOL ERROR: Empty pool");
        return null;
    }

    public bool ReturnElement(GameObject returnedElement)
    {
        if (m_Pool.Contains(returnedElement))
        {
            Debug.Log(returnedElement.name + " -> RETURN");
            returnedElement.SetActive(false);
            return true;
        }

        Debug.Log("EXCEPTION POOL ERROR: GameObject not in pool");
        return false;

    }

    public void doPool(bool status)
    {
        if (status)
            GetElement();
        else
        {
            for (int i = Capacity - 1; i >= 0; i--)
            {
                //Debug.Log("" + m_Pool[i]);
                if (m_Pool[i].gameObject.activeInHierarchy)
                {
                    ReturnElement(m_Pool[i].gameObject);
                    return;
                }
            }
            //foreach (GameObject element in m_Pool)
            //{
            //    if (element.activeInHierarchy)
            //    {
            //        ReturnElement(element);
            //        return;
            //    }
            //}
        }
    }
}
